.NOLIST
#include "ti83plus.inc"

Enter    .EQU 05h
Clear    .EQU 09h

.LIST
.ORG $9D93                      ; program start address
    .db $BB, $6D                ; 2 bytes to signal TI-OS
                                ; this is an ASM program
Init:
    ld  a, (8447h)              ; stores current contrast value
                                ; from addr 8447h into reg. a and 
    ld (curcontrast), a         ; then into variable curcontrast

    bcall(_ClrLCDFull)
    bcall(_RunIndicOff)         ; kill the run indicator
    ld a,0
    ld (curcol), a              ; initialize cursor col,row
    ld (currow), a              ; to 0,0

Main:
    call Init   
 
    ld hl, prompt
    bcall(_puts)
    bcall(_getKey)
    ld b,$8E                    ; loads 142 in B (number offset)
    sub b                       ; subtracts number offset from 
                                ; keycode stored in A by _getKey
                                ; to obtain number pressed

    ld b,a                      ; stores number pressed in B

    bcall(_getkey)              ;checks for keys
    
    cp   Enter                  ;on Enter -> Restore the current contrast back in RAM
    jr   z,ChangeContrast
    
    cp   Clear                  ;on Clear -> Don't restore current contrast back in RAM
    jr   nz,Main                ;but reload old contrast back from RAM and apply that
    ld   a,(8447h)              ;load the current contrast from RAM causes you pushed CLEAR ...
    jr   Do

    ret

;DrawContrast:
;    ld  hl,$0404
;    ld  a,0 
;    ld  (curcol),a
;    ld  (currow),a
;    ld hl, (curcontrast)
;    bcall(_puts)
;    ld  a,(curcontrast)
;    ld  h,0
;    ld  l,a
;    bcall(_disphl)
;    ret

ChangeContrast:
    ld  a,b  
    ld  (8447h),a              ; loads new contrast back into RAM
    call Do

SetContrast:                    ; apply the contrast to the LCD (port 10h)
     add  a,$18
     or   $c0
     call LcdBusy
     out  (10h),a
;     call DrawContrast
     ret

LcdBusy:                        ; small delay for the display driver
     push hl
     inc  hl
     dec  hl
     pop  hl
     ret

Do:
    call SetContrast
    ret

Exit:
    ret

Prompt:
    .db ": ", 0

curcontrast .db 00h

.END
END
