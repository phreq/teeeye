The Contrast Utility
--------------------
by:	JeePee
e-mail:	jorispeeraer@tiscali.be
date:	15-06-03  14:45

WHAT?
-----
This source is of a program to change to contrast (made by myself :-D)
I made it because I've looked everywhere, but I didn't find anything that worded correctly.

USAGE:
------
[+] and [-] change the contrast by 1.
[ENTER] sets the current contrast and saves it back into RAM
[CLEAR] doesn't set the current contrast, but reloads the old contrast.

DISCLAIMER:
-----------
You may use it freely in your programs, but give me some credits plz....