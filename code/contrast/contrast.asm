.NOLIST
#include  "ti83plus.inc"
#define   bcall(label)   rst 28h \ .dw label
Min      .EQU 81h
Plus     .EQU 80h
Enter    .EQU 05h
Clear    .EQU 09h
.LIST

     .org 9D93h
     .db  $BB,$6D
     
     ld   a,(8447h)                     ;load the current contrast from RAM
     ld   (curcontrast),a               ;load it into a temporary variable
     call drawcontrast
mainloop:
     bcall(_getkey)                     ;checks for keys
     cp   Enter                         ;on Enter -> Restore the current contrast back in RAM
     jr   z,Change
     cp   Min                           ;on Min   -> Decrese the contrast with 1
     jr   z,Minder
     cp   Plus                          ;on Plus  -> Increase the contrast with 1
     jr   z,Meer
     cp   Clear                         ;on Clear -> Don't restore current contrast back in RAM
     jr   nz,mainloop                   ;but reload old contrast back from RAM and apply that
     ld   a,(8447h)                     ;load the current contrast from RAM causes you pushed CLEAR ...
     jr   Do
Change:
     ld   a,(curcontrast)               ;load the current contrast ...
     ld   (8447h),a                     ;... load it back into RAM ...
Do:
     call SetContrast                   ;... and apply it to the LCD
     ret
Minder:                                 ;Decrease the current contrast
     ld   a,(curcontrast)
     cp   0h
     jr   z,mainloop
     dec  a
     ld   (curcontrast),a
     call SetContrast
     jr   mainloop
Meer:                                   ;increase the current contrast
     ld   a,(curcontrast)
     cp   39d
     jr   z,mainloop
     inc  a
     ld   (curcontrast),a
     call SetContrast
     jr   mainloop
     
SetContrast:                            ;Apply the contrast to the LCD (port 10h)
     add  a,$18
     or   $c0
     call lcdbusy
     out  (10h),a
     call drawcontrast
     ret
drawcontrast:
     ld   hl,$0404
     ld   (currow),hl
     ld   a,(curcontrast)
     ld   h,0
     ld   l,a
     bcall(_disphl)
     ret
lcdbusy:                                ;Smal Delay for the display driver
     push hl
     inc  hl
     dec  hl
     pop  hl
     ret

curcontrast  .db 00h                    ;Temporary variable to store the contrast during the program
.END
.END