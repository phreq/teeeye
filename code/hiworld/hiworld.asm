.nolist
#include "ti83plus.inc"
.list
.org $9D93                      ; program start address
    .db $BB, $6D                ; 2 bytes to signal TI-OS
                                ; this is an ASM program
Start:
    ;bcall(_ClrLCDFull)
    ld a, 0                      
    ld (curcol), a              ; initialize cursor col,row
    ld (currow), a              ; to 0,0
    ld hl, HelloWorldMessage
    bcall(_puts)
    bcall(_newline)
    ret
HelloWorldMessage:
    .db "Hello, World!", 0
.end
END
