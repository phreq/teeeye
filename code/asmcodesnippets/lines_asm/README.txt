Grtz!


This is a library I made from some routines I wrote for the TI83+ calculator family and that I commonly use. I use ZDS 3.68, so I'm not sure this code works well with other compilers without being altered. The main purpose is to share code that's hardly written, but is of use in many cases (For example, drawing and deleting lines is VERY usefull if you want to draw a graphical interface). The code snipets included in this package don't not use any system routine of the calculator to asure optimal speed. Also, they can be used both for asm-programmes and flash applications. If you find any bugs, made some improvements or just want to say thanks feel free to e-mail me at wimvhs02@gmail.com. Also people who are interested in adding code snippets of themselves can send them to me, and I will include them with the name of the author! People that said thanks might also be included in a special credits file.



HOW TO USE the code snipets: this is descibed in a header that each snipet has got on top of the file.



I will give you a short description of the files here:

* Draw_Or_Delete_Vertical_Line.asm			: This contains the code to draw or delete a vertical line
* Draw_Or_Delete_Horizontal_Line.asm			: This contains the code to draw or delete a horizontal line



More code snipets will follow.



with kind regards,

Wim VHS,
e-mail: wimvhs02@gmail.com


PS: if you use any of these code snipets, please mention me or the author's name.