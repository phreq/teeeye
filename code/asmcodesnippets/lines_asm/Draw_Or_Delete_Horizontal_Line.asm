;****************************************
;* Draw or delete a horizontal line     *
;*--------------------------------------*
;* Written by Wim VHS, 30/06/2009 10:18 *
;* e-mail: wimvhs02@gmail.com				 *
;****************************************



; This code snipet contains a routine to draw or delete horizontal lines between two screen coordinates. Comments are in Dutch.


; HOW TO USE:
; 		To use this code, you have to include this file with your source file, by adding include "DrawHorizontalLine.asm" at the end of
;		your file (befor .end). Whenever you want to draw a line you add a CALL DrawHorizontalLine instruction to your source, and
;		similarly if you want to delete a Line, you add a CALL DeleteHorizontalLine instruction to your source code.

; The INPUT REGISTERS are the following:
; * D: x-coordinate of the beginning of the line
; * E: y-coordinate of the beginning of the line
; * H: x-coordinate of the end of the line

; This routine DESTROYS the A,F,B,C&D registers





;****************************************************************************************************
;* Teken een horizontale lijn 																							 *
;*--------------------------------------------------------------------------------------------------*
;* Input: a=aantal aan te zetten pixels eerste byte van de lijn d=x-co begin lijn e=y-co begin lijn *
;*			 h=x-co einde lijn (geen y-co einde lijn nodig, horizontale lijn)									 *
;* Output: horizontale lijn	  																							 *
;* Vernietigt: a, f, b, c, d, HL BLIJFT INTACT!!!																	 *
;****************************************************************************************************


DeleteHorizontalLine:
 xor a
 jr WisHorizontaleLijn_Label01

DrawHorizontalLine:

 ld a,01
WisHorizontaleLijn_Label01:
 push af

 call DeelDDoor8
 ld b,a
 ld a,07h
 call WachtLCD
 out (10h),a
 ld a,20h
 add a,d
 call WachtLCD
 out (10h),a
 ld a,80h
 add e
 call WachtLCD
 out (10h),a

; Deel D door 8 om te weten in welke byte de lijn begint. De rest A is het aantal pixels dat in die byte NIET aan
; moet (door deze pixels te berekenen en dan een NOT A te doen komen we bij het juiste uit, maar dat gebeurt
; later). Sla A op in B om later een DJNZ-Loop te kunnen doen. Zet het scherm op y-autoincrement (de y-as van het
; scherm = x-as wiskunde), zet de beginco�rdinaten van de lijn.


 xor a

 dec d	;nodig voor als men direct een volledig stuk lijn mag tekenen
 
 cp b
 jr z,TekenHorizontaleLn_Label02

 inc d   ; niet nodig als men niet meteen een volledig stuk lijn mag tekenen.

TekenHorizontaleLn_Loop01:
 scf								; set de carryflag
 rra
 djnz TekenHorizontaleLn_Loop01
 cpl								; = not a
 ld c,a

 pop af
 push af
 or a
 jr nz, WisHorizontaleLijn_Label02
 ld a,c
 cpl
 ld c,a

; Check of NIET alle pixels AAN moeten (B is het aantal pixels dat UIT moet), en als dit niet is, spring dan naar
; TekenHorizontaleLn_Label02. Moeten er wel aan, doe dan een DJNZ-Loop waarin de carry-flag steeds aangezet wordt
; (SCF) en dan in bit 7 van het scherm geshift wordt. Door na de loop NOT A (CPL) te doen komen we dan bij het
; juiste begin van de lijn uit. Sla dit begin op in C.


WisHorizontaleLijn_Label02:

 call WachtLCD
 in a,(11h)

 pop af
 push af
 or a
 jr nz,WisHorizontaleLijn_Label03
 call WachtLCD
 in a,(11h)
 and c
 jr WisHorizontaleLijn_Label04

WisHorizontaleLijn_Label03:

 call WachtLCD
 in a,(11h)
 or c

WisHorizontaleLijn_Label04:

 push af
 ld a,20h
 add d
 call WachtLCD
 out (10h),a
 pop af
 call WachtLCD
 out (11h),a

; Er wordt eerst een dummy read gedaan en de wordt de byte die al op het scherm staat ingelezen en via een OR C
; bewaardt om later weer op het scherm weer te geven (dit is onder andere nodig als er tekst valt binnen de byte
; die afgeprint wordt). De Y-co vh scherm (=X-co wis) wordt hersteld en de byte geprint.


TekenHorizontaleLn_Label02:
 push de
 ld d,h
 call DeelDDoor8
 ld c,a
 ld a,d
 pop de
 sub d
 jr z,TekenHorizontaleLijn_Label01

 dec a ; nodig omdat er nu een deel minder volledig byte moet worden getekent dan wanneer er geen pixels aan zouden moeten in byte 1
 jr z,WisHorizontaleLijn_Label13 ; als er maar D slechts 1 kleiner was dan H, dan geen volledige rij die getekent/gewist moet worden.

 ld b,a

 pop af
 push af
 or a
 jr nz,WisHorizontaleLijn_Label05
 xor a
 jr TekenHorizontaleLn_Loop02

WisHorizontaleLijn_Label05:

 ld a,11111111b

TekenHorizontaleLn_Loop02:
 call WachtLCD
 out (11h),a
 djnz TekenHorizontaleLn_Loop02

; DE wordt opgeslagen, en H gedeeld door 8. De rest A is het aantal pixels dat op het einde van de lijn nog aan
; moet en wordt in C opgeslagen. Het quoti�nt D wordt in A geladen, DE herstelt en dan wordt D van A afgetrokken.
; Het verschil is het aantal bytes met alle pixels aan die moeten worden geprint. Zijn dit er geen, dan wordt naar
; TekenHorizontaleLijn_Label01 gejumpt. Het verschil wordt in B geladen om met een DJNZ-loop de bytes te kunnen
; printen.


WisHorizontaleLijn_Label13:

 inc c
 ld b,c
 xor a
 cp b
 
 jr nz,WisHorizontaleLijn_Label12
 
 pop af
 ret

WisHorizontaleLijn_Label12:
 
 call WachtLCD
 in a,(11h)
 call WachtLCD
 in a,(11h)
 ld c,a
 xor a
TekenHorizontaleLn_Loop03:
 inc a
 rrca
 djnz TekenHorizontaleLn_Loop03
 
 ld e,a
 pop af
 push af
 or a
 jr nz,WisHorizontaleLijn_Label07
 ld a,e
 cpl
 and c
 jr WisHorizontaleLijn_Label08

WisHorizontaleLijn_Label07:
 ld a,e
 
 or c

WisHorizontaleLijn_Label08:

 push af
 ld d,h
 call DeelDDoor8
 ld a,20h
 add d
 call WachtLCD
 out (10h),a
 pop af
 call WachtLCD
 out (11h),a

 pop af

 ret

; Er wordt 1 opgeteld bij C om de lijn te tekenen tot EN MET de eindcoordinaat. Dan wordt dit in B geladen om de
; pixels met een DJNZ-Loop te tekenen. Er wordt gecheckt of er ten minste ��n pixel aan moet, en is dat niet zo
; dan wordt er gestopt want dan is het einde van de lijn bereikt. Er wordt een dummy read gedaan en dan wordt het
; scherm echt gelezen om een OR te kunnen doen met de pixels die al aan staan. A wordt gereset en in een DJNZ-Loop
; wordt met INC A en RRCA de lijn getekend (=SCF en dan RRA). Dan wordt er een OR C gedaan, de coordinaten
; hersteld en de byte wordt getekend. De lijn is hier getekend, dus RET.


TekenHorizontaleLijn_Label01:
 ld b,c
 inc b
 xor a
TekenHorizontaleLn_Loop04:
 inc a
 rrca
 djnz TekenHorizontaleLn_Loop04
 ld c,a

 pop af
 push af
 or a
 jr nz,WisHorizontaleLijn_Label09
 ld a,c
 cpl
 ld c,a
WisHorizontaleLijn_Label09:

 ld a,20h
 add d
 call WachtLCD
 out (10h),a
 ld b,a
 call WachtLCD
 in a,(11h)

 pop af
 push af
 or a
 jr nz,WisHorizontaleLijn_Label10
 call WachtLCD
 in a,(11h)
 or c
 jr WisHorizontaleLijn_Label11
WisHorizontaleLijn_Label10:

 call WachtLCD
 in a,(11h)
 and c

WisHorizontaleLijn_Label11:

 ld c,a
 ld a,b
 call WachtLCD
 out (10h),a
 ld a,c
 call WachtLCD
 out (11h),a

 pop af

 ret

; Deze subroutine handelt een horizontale lijn af wanneer ze begint en eindigt in dezelfde schermbyte. De pixels
; die aan het begin van een lijn, die uit meerdere bytes bestaat, staan, zijn al op het scherm geprint en C
; bevat het aantal pixels die bij een meerdere-bytes lijn op het einde geprint moeten worden. B wordt in C geladen
; voor een DJNZ-Loop en er wordt INC B gedaan, een foutcorrectie. A wordt gereset en het einde van de lijn wordt
; berekent. Dit wordt in C opgeslagen, en de schermco wordt op het begin van de lijn gezet en opgeslagen in B om
; later te kunnen herstellen. Er wordt een dummy read gedaan en dan wordt er echt gelezen. Door AND C worden de
; pixels die aan moeten blijven staan bewaart (dit zijn de pixels die het begin en het einde van de lijn
; gemeenschappelijk hebben). Eventuele andere lijnen die door deze byte lopen worden ook bewaart. De schermco
; wordt hersteld en de pixels geprint op het scherm.



;=================================================================================================================
;=================================================================================================================
;=================================================================================================================
;=================================================================================================================
;=================================================================================================================
