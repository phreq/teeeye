;****************************************
;* Draw or delete a vertical line       *
;*--------------------------------------*
;* Written by Wim VHS, 30/06/2009 10:18 *
;* e-mail: wimvhs02@gmail.com				 *
;****************************************



; This code snipet contains a routine to draw or delete vertical lines between two screen coordinates. Comments are in Dutch.


; HOW TO USE:
; 		To use this code, you have to include this file with your source file, by adding include "DrawVerticalLine.asm" at the end of
;		your file (befor .end). Whenever you want to draw a line you add a CALL DrawVerticalLine instruction to your source, and
;		similarly if you want to delete a Line, you add a CALL DeleteVerticalLine instruction to your source code.

; The INPUT REGISTERS are the following:
; * D: x-coordinate of the beginning of the line
; * E: y-coordinate of the beginning of the line
; * L: y-coordinate of the end of the line (the x-coordinate of the end isn't necessary)

; This routine DESTROYS the A,F,B,C,D&E registers





;****************************************************************************************************
;* Teken een verticale lijn 																								 *
;*--------------------------------------------------------------------------------------------------*
;* Input: a=aantal aan te zetten pixels eerste byte van de lijn d=x-co begin lijn e=y-co begin lijn *
;*			 l=y-co einde lijn (geen x-co einde lijn nodig, horizontale lijn)									 *
;* Output: verticale lijn		  																							 *
;* Vernietigt: a, f, b, c, d, e  																						 *
;****************************************************************************************************


DeleteVerticalLine:
 xor a
 jr TekenVerticaleLijn_Label01
 
DrawVerticalLine:
 ld a,01

TekenVerticaleLijn_Label01:
 push af
 
 call DeelDDoor8
 ld b,a
 xor a
 inc a
 rrca
TekenVerticaleLijn_Loop01:								
 rrca
 djnz TekenVerticaleLijn_Loop01
 ld c,a
 
 pop af
 push af
 or a
 jr nz,TekenVerticaleLijn_Label02
 ld a,c
 cpl
 ld c,a
TekenVerticaleLijn_Label02:


; D wordt door 8 gedeeld om de kolombyte te kennen. De rest A is het bitnummer (van rechts geteld, dus bit 7=bit 0
; , bit 6=bit 1) van de pixel die aan moet. A wordt in B geladen voor een DJNZ. A wordt gereset en bit 7 aangezet
; om te kunnen beginnen shiften. Er wordt een initialiserings RRCA gedaan (ik weet niet waarom, maar dit is nodig
; en het komt wel juist uit, ook als de lijn op y-co van het scherm (=x-co wis) moet staan). Via een loop wordt
; er dan een model gemaakt en dit wordt in C opgeslagen.


 ld a,05h
 call WachtLCD
 out (10h),a
 ld a,20h
 add d
 call WachtLCD
 out (10h),a
 ld a,80h
 add e
 ld d,a
 call WachtLCD
 out (10h),a

; Om de lijn juist te tekenen moet het scherm op x-autoincrement staan (van boven naar beneden). Dan worden de
; coordinaten in het scherm gezet. De beginrij wordt in D opgeslagen, om later bij het lezen de rij te kunnen
; herstellen.


 ld a,L
 sub e
 ld b,a
 inc b
TekenVerticaleLijn_Loop02:
 call WachtLCD
 in a,(11h)
 call WachtLCD
 in a,(11h)
 
 ld e,a
 pop af
 push af
 or a
 ld a,e
 jr nz,TekenVerticaleLijn_Label03
 and c
 jr TekenVerticaleLijn_Label04
 TekenVerticaleLijn_Label03:

 or c
 
TekenVerticaleLijn_Label04:
 
 ld e,a
 ld a,d
 call WachtLCD
 out (10h),a
 ld a,e
 call WachtLCD
 out (11h),a
 inc d
 djnz TekenVerticaleLijn_Loop02

 pop af

 ret

; De eindrij wordt van de beginrij afgetrokken om te weten hoe lang de lijn moet worden en dit verschil wordt in B
; opgeslagen voor een DJNZ. Er wordt INC B gedaan om de scherm tot EN MET de eindco te tekenen. In de Loop wordt
; er eerst een dummy read gedaan en dan wordt het scherm echt gelezen. Met een OR met het model wordt vermeden dat
; pixels die al op het scherm stonden verloren gaan. Dan wordt het actieve rijnummer in D hersteld, en dan wordt
; de lijn getekend. Er wordt INC D gedaan op het einde van de Loop, het rijnummer verandert. Na de loop is de lijn
; getekend, dus RET.



;=================================================================================================================
;=================================================================================================================
;=================================================================================================================
;=================================================================================================================
;=================================================================================================================