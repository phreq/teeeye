.org $9D93
#include "ti83plus.inc"
start:
    ld hl,0
    ld (currow),hl
    
    ld c,$48
    IN h, (C)
    DEC C
    IN l, (C)


Div_HL_c:            ; HL = HL � C, A = remainder
    XOR    A         ; Clear upper eight bits of AHL
    ld     bc, $103C ;b:16 bits to rotate, c:60 to divide by 60 (seconds)
_loop:
    ADD    HL, HL     ; Do a SLA HL
    RLA              ; This moves the upper bits of the dividend into A
    ;JR     C, _overflow
    CP     C         ; Check if we can subtract the divisor
    JR     C, _skip   ; Carry means c > A
_overflow:
    SUB    C         ; Do subtraction for real this time
    INC    L         ; Set bit 0 of quotient
_skip:
    DJNZ   _loop

    push hl
    
    
    
    
    
    ld c,$46
    IN h, (C)
    dec c
    IN l, (C)

    

Div_HL_c2:            ; A = remainder keep the remailner of the 2 most significant bytes
     ld     bc, $103C ;b:16 bits to rotate, c:60 to divide by 60 (seconds)
_loop2:
    ADD    HL, HL     ; Do a SLA HL
    RLA              ; This moves the upper bits of the dividend into A
    CP     C         ; Check if we can subtract the divisor
    JR     C, _skip2   ; Carry means D > A
_overflow2:
    SUB    C         ; Do subtraction for real this time
    INC    L         ; Set bit 0 of quotient
_skip2:
    DJNZ   _loop2
    
    push hl
    

    ld h,0
    ld l,a
    bcall(_disphl)  ;display seconds



    bcall(_newline)
    pop hl
    ex (sp),hl

Div_HL_c3:            ; HL = HL � C, A = remainder
    XOR    A         ; Clear upper eight bits of AHL
    ld     bc, $103C ;b:16 bits to rotate, c:60 to divide by 60 (minutes)
_loop3:
    ADD    HL, HL     ; Do a SLA HL
    RLA              ; This moves the upper bits of the dividend into A
    CP     C         ; Check if we can subtract the divisor
    JR     C, _skip3   ; Carry means c > A
_overflow3:
    SUB    C         ; Do subtraction for real this time
    INC    L         ; Set bit 0 of quotient
_skip3:
    DJNZ   _loop3
    
    ex (sp),hl
    

Div_HL_c4:            ; HL = HL � C, A = remainder
    ;XOR    A         ; Clear upper eight bits of AHL
    ld     bc, $103C ;b:16 bits to rotate, c:60 to divide by 60  (minutes)
_loop4
    ADD    HL, HL     ; Do a SLA HL
    RLA              ; This moves the upper bits of the dividend into A
    ;JR     C, _overflow4
    CP     C         ; Check if we can subtract the divisor
    JR     C, _skip4   ; Carry means c > A
_overflow4:
    SUB    C         ; Do subtraction for real this time
    INC    L         ; Set bit 0 of quotient
_skip4:
    DJNZ   _loop4
    
    push hl    

    ld h,0
    ld l,a
    bcall(_disphl)  ;display minutes
    
    bcall(_newline)
    pop hl
    ex (sp),hl

Div_HL_c5:            ; L = L � C, A = remainder
    XOR    A         ; Clear upper eight bits of AHL
    ld     bc, $0818 ;b:8 bits to rotate, c:24 to divide by 24 (houwers)
_loop5:
    SLA     L     ; Do a SLA L
    RLA              ; This moves the upper bits of the dividend into A
    ;JR     C, _overflow5
    CP     C         ; Check if we can subtract the divisor
    JR     C, _skip5   ; Carry means c > A
_overflow5:
    SUB    C         ; Do subtraction for real this time
    INC    L         ; Set bit 0 of quotient
_skip5:
    DJNZ   _loop5
    
    pop hl
    

Div_HL_c6:            ; HL = HL � C, A = remainder
    ;XOR    A         ; Clear upper eight bits of AHL
    ld     bc, $1018      ;b:16 bits to rotate, c:24 to divide by 24 (houwers)
_loop6
    ADD    HL, HL     ; Do a SLA HL
    RLA              ; This moves the upper bits of the dividend into A
    CP     C         ; Check if we can subtract the divisor
    JR     C, _skip6   ; Carry means C > A
_overflow6:
    SUB    C         ; Do subtraction for real this time
    INC    L         ; Set bit 0 of quotient
_skip6:
    DJNZ   _loop6
    
    push hl    

    ld h,0
    ld l,a
    bcall(_disphl)   ;display hours
    bcall(_newline)
    ld de,0
    pop hl
    or a
    ld  bc,1095  ;remove 3 years so we can start at 2000
    sbc hl,bc
    ex  de,hl
Div_HL_bc7:            ; DE = DE � BC, HL = remainder
;    ld     hl,0
    ld     A,16         
    ld     bc, $5b5      ; 1461 = 365.25*4 so we will know howmaly iterclary years(=leap year) are pased since 2000 (and count the 29st of februays)
_loop7
    SLA    E
    RL     D           ; This moves the upper bits of the dividend into HL
    ADC    hl,hl
    push   hl
    sbc    hl,bc        ; Check if we can subtract the divisor
    pop    hl
    JR     C, _skip7
_overflow7:
    SBC    hl,BC         ; Do subtraction for real this time
    INC    E         ; Set bit 0 of quotient
_skip7:
    dec a
    jr   nz,_loop7
    

    sla  e  ;DE*4 to get the real year as we counted the leap years
    sla  e

    ex de,hl
    push hl
            
Div_HL_bc8:            ; DE = DE � BC, HL = remainder
    ld     hl,0
    ld     A,16         ; Clear upper eight bits of HLDE
    ld     bc, $16D  ;ld bc,365    get the amout of years after the last leap year
_loop8
    SLA    E
    RL     D           ; This moves the upper bits of the dividend into HL
    ADC    hl,hl
    push   hl
    sbc    hl,bc        ; Check if we can subtract the divisor
    pop    hl
    JR     C, _skip8   ; Carry means BC > HL
_overflow8:
    SBC    hl,BC         ; Do subtraction for real this time
    INC    E         ; Set bit 0 of quotient
_skip8:
    dec a
    jr   nz,_loop8
    


    ld a,e
    or a
    jr nz,skip291
    inc hl
    inc d
    jr skip29
skip291:

    
    ld a,l
    or h
    jr nz,skip29
    dec e
    ld hl,365

    ld a,e
    or a
    jr nz,skip291
    inc hl
    inc d
    

skip29:


    pop bc
    ld  a,e

    add a,c

    ld  e,a

    push de
    push hl

    
    ;ld b,0
    ld c,e
    ld hl,2000 ; here the amout of years has his normal form and can be displayed
    add hl,bc
    bcall(_disphl)     ;dispaly year
    bcall(_newline)
    
    pop hl              ;this counts hl down to find the current month
    ld de,1
    ld bc,31
    sbc hl,bc
    jr c,monthfound   ;jan.
    inc e
    
    ld c,28
    pop af
    add a,c ;add 1 if it is a leap year
    ld c,a
    sbc hl,bc
    jr c,monthfound   ;feb.
    inc e
    
    ld c,31
    sbc hl,bc
    jr c,monthfound   ;march
    inc e
    
    
    dec c
    sbc hl,bc
    jr c,monthfound    ;april
    inc e
    
    inc c
    sbc hl,bc
    jr c,monthfound    ;may
    inc e
    
    dec c
    sbc hl,bc
    jr c,monthfound    ;june
    inc e
    
    inc c
    sbc hl,bc
    jr c,monthfound    ;july
    inc e
    
    sbc hl,bc
    jr c,monthfound    ;aug.
    inc e
    
    dec c
    sbc hl,bc
    jr c,monthfound    ;sept.
    inc e
    
    inc c
    sbc hl,bc
    jr c,monthfound    ;oct.
    inc e
    
    dec c
    inc e
    sbc hl,bc
    jr  nc,monthfound1    ;dec.
    dec e
    ;                     nov
monthfound:
    add hl,bc
monthfound1:
    push hl
    ex de,hl          ;ld hl,de because month is in de
    bcall(_disphl)        ;display month
    bcall(_newline)
    pop hl
    inc l
    bcall(_disphl)         ;display day

    bcall(_getcsc)
    or a
    jp z,start
    ret
.end



