Clock and Date
made by: Igor Voulis
email:Igor.Voulis@gmail.com
created on: 02/09/2006
posted on: 01/03/2007 (forgot to do it)

'Clock and date' (TIMEDATE on the calculator) is an asm program that decodes the seconds/minutes/hours/year/moths/day from the ports ($45-$46-$47-$48) of the TI-84plus(se), it works from 2000 till 2100 as there will not be any TI84plus(se) left in 2100. However I could make it possible to decode it from 1997 to 2132 but it woud take longer and it wouldn't make it usefuller.
I have released this so people who want to use the time and date in an asm program wouldn't need spend hours and hours to figure it all out. I hope it will be usefull. If you use it would be nice to mention me in corner of the readme of your file where nobuddy looks anyway. Note that I'm not resposable for any damage caused by this program to you or your calculator.

Mail me questions, ideas, comments... on Igor[dot]Voulis[at]gmail[dot]com (Igor.Voulis@gmail.com that is).

