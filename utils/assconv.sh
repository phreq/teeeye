#!/bin/sh

if [ "$#" -ne 1 ]
then
    echo -e "Too few arguments"
    echo -e "Usage: assconv.sh <source filename without extension>"
    echo -e "the source file should be saved as .asm"
    exit
fi
 
INC_DIR=/home/morra/ti84/code/includes
BINPAC8X_DIR=/home/morra/ti84/utils

spasm -E $1.asm -I $INC_DIR
python $BINPAC8X_DIR/binpac8x.py $1.bin $1.8xp

TILP_FLAG=0

if tilp -n | grep -q "errno = 11" 
then
    echo -e "Failed to open USB device"
    echo -e "Check that the USB cable is plugged in and that the calculator is turned ON!"
    exit
else
    TILP_FLAG=1
    tilp -n -s --calc=ti84+ --cable=DirectLink -p 1 $1.8xp
fi
